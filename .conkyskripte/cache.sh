#!/bin/bash
a=$(grep ^Cached /proc/meminfo|awk {'print $2'})
b=$(grep ^MemTotal /proc/meminfo|awk {'print $2'})
echo "scale=5; $a. / $b. *100"|bc
exit

