if [[ $HOST == "t460p" ]]; then
  dp=$(xrandr|grep -E "[^e]DP" |grep " connected")
  hdmi=$(xrandr|grep HDMI      |grep " connected")
  if [[ $dp && $hdmi ]]; then
    ~/.screenlayout/t460p-DP-HDMI.sh
  elif [[ $dp ]]; then
    ~/.screenlayout/t460p-rechts-DP.sh
  elif [[ $hdmi ]]; then
    ~/.screenlayout/t460p-rechts-HDMI.sh
  else
    ~/.screenlayout/t460p-laptop.sh
  fi
elif [[ cat ~/.screenlayout/$HOST.sh ]]; then
  ~/.screenlayout/$HOST.sh
else
  r=$(xrandr|grep VGA|grep disconnected)
  if [[ $r == "" ]]; then
    ~/.screenlayout/rechtermonitor.sh
  else
    ~/.screenlayout/laptop.sh
  fi
fi
killall conky
killall nm-applet
nm-applet &
killall volti
volti &
dropbox start
conky &
