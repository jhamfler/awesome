#!/bin/bash
wd=$(pwd)
cp .conkyrc .conkyrctemp


#colors
blue="#0088bb"
white="#eeeeee"


cd /sys/class/power_supply/
batterie=$(ls -1 */*_full|egrep -o '.+\/'|sed -e 's/\///g')
sed -i "s/BAT0/$batterie/g" "$wd"/.conkyrctemp

cd ~/.conkyskripte/
sed -i "s/BAT0/$batterie/g" batt-p.sh

# WRITE CONKY FILE
cat >"$wd"/.conkyrctemp <<EOL
# grenzen
border_width 0
border_inner_margin 0
border_outer_margin 0
stippled_borders 0

#experimentell
default_bar_size 350 9
default_gauge_size 25 25
default_graph_size 10 25

#extra zeugs
draw_shades no

#musik
#mpd server
#mpd_host localhost
#mpd_password
#mpd_port 6000

#für awesome innerhalb einer wibox
#extra_newline yes

# performance
update_interval 1.0
update_interval_on_battery 3.0
diskio_avg_samples 3
cpu_avg_samples 3
net_avg_samples 1
#zeige RAM ohne Puffer an
no_buffers yes

# einzelne cores
#top
top_cpu_separate yes
top_name_width 20
#leerzeichen links einfügen
use_spacer yes

default_color 777777
default_outline_color 222222
draw_borders no
draw_graph_borders yes
draw_outline no
draw_shades no

# nice fonts shitty process list when yes
use_xft no
xftfont Sans Mono:size=9

# X font when Xft is disabled, you can pick one with program xfontsel
#font 5x7
#font 6x10
#font 7x13
#font 8x13
#font 9x15
#font *mintsmild.se*
#font -*-*-*-*-*-*-34-*-*-*-*-*-*-*
#font -bitstream-courier 10 pitch-bold-r-normal-*-17-120-*-*-m-0-ascii-0
#font -*-*-*-*-*-*-14-*-*-*-*-*-*-*
font -*-clean-*-*-*-*-14-*-*-*-*-*-*-*

out_to_console no
out_to_stderr no
extra_newline no

# fensteroptionen
alignment top_left
own_window yes
own_window_class Conky
#own_window_colour ff0000
own_window_transparent no
#own_window_hints undecorated,below,sticky,skip_pager
own_window_hints below,sticky,skip_pager
minimum_size 1366 10
maximum_width 1366
gap_x 1
gap_y 20
#own_window_type desktop
own_window_type override

# flackern unterdrücken
double_buffer yes

stippled_borders 0
uppercase no
show_graph_scale no
show_graph_range no
format_human_readable yes
own_window_transparent no

#temperature_unit celsius
#GiB -> G KiB -> k
short_units no

#\${font Dungeon:style=Bold:pixelsize=20}

TEXT
\${color ${blue}}CPU:\${ color ${white}}\${cpubar cpu0 9,25} \${freq_g 0}\
\${color ${blue}} RAM:\${color ${white}}\${membar 9,25} \${mem}\
\
\
\${color ${blue}} Temp:\${color ${white}}\
\${if_match \${acpitemp}>=1}\${acpitemp}C\
\${else}\
\
\${if_match \${execi 5 sensors|grep "Core 0"|awk '{print \$3}'|egrep -o '[0-9]+\.'|egrep -o '[0-9]+'}>=1}\
\${execi 5 sensors|grep "Core 0"|awk '{print \$3}'|egrep -o '[0-9]+\.'|egrep -o '[0-9]+'}C\
\${else}\
\






EOL
