#!/bin/bash
if [[ $(cat /etc/hostname) == "t460p" ]]; then
  dp=$(xrandr|grep -E "[^e]DP" |grep " connected")
  hdmi=$(xrandr|grep HDMI        |grep " connected")
  if [[ $dp && $hdmi ]]; then
    ~/.screenlayout/t460p-DP-HDMI.sh
  elif [[ $dp ]]; then
    ~/.screenlayout/t460p-Ldp4k.sh
  elif [[ $hdmi ]]; then
    ~/.screenlayout/t460p-Lhdmi1k.sh
  else
    ~/.screenlayout/t460p-laptop.sh
  fi
elif [[ -f ~/.screenlayout/$(cat /etc/hostname).sh ]]; then
  ~/.screenlayout/$(cat /etc/hostname).sh
else
  r=$(xrandr|grep VGA|grep disconnected)
  if [[ $r == "" ]]; then
    ~/.screenlayout/rechtermonitor.sh
  else
    ~/.screenlayout/laptop.sh
  fi
fi
killall nm-applet
killall conky
killall albert
nm-applet &
volti &
dropbox start
conky &
albert &
